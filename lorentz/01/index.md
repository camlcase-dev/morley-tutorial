# Part I: Hello Lorentz

## Starting a new project

Make sure you have stack (a GHC Haskell build tool) installed. If you do not, 
follow the [official documentation](https://docs.haskellstack.org/en/stable/README/).

Start a new project `stack new hello-lorentz`. This creates a new Haskell 
package in a directory called `hello-lorentz` with a simple library, executable 
and test suite.

Go into the directory and edit the `stack.yaml` file. We are using the latest
version directly (or any particular commit) from the 
[GitLab repository](https://gitlab.com/morley-framework/morley).

### stack.yaml for latest commit as of 2020-06-02

```yaml
resolver: lts-15.7

extra-deps:
- aeson-options-0.1.0
- base-noprelude-4.13.0.0
- base58-bytestring-0.1.0
- constraints-0.11
- constraints-extras-0.3.0.2
- dependent-sum-0.7.1.0
- dependent-sum-template-0.1.0.3
- fmt-0.6.1.2
- hashing-0.1.0.1
- hex-text-0.1.0.0
- monoidal-containers-0.6.0.1
- named-0.3.0.1
- pretty-terminal-0.1.0.0
- show-type-0.1.1
- vinyl-0.12.2
- witherable-0.3.5
- witherable-class-0

- git: https://gitlab.com/obsidian.systems/tezos-bake-monitor-lib.git
  commit: ed3280836d7d0e87999639520610bf7b6510e743
  subdirs:
    - tezos-bake-monitor-lib
- git: https://gitlab.com/morley-framework/morley.git
  commit: 34b82c953943c9eee77a94a437105109d5b90cd0
  subdirs:
    - code/indigo
    - code/indigo/tutorial
    - code/lorentz
    - code/morley
    - code/morley-client
    - code/morley-ledgers
    - code/morley-ledgers-test
    - code/morley-multisig
    - code/morley-nettest
    - code/morley-prelude
    - code/morley-upgradeable
    - code/morley-debugger
    - code/tasty-hunit-compat
```

Now we need to make a small update to the `package.yaml` file. Replace the top
level `dependencies` clause:

```yaml
dependencies:
- base >= 4.7 && < 5
```

with:

```yaml
dependencies:
- base-noprelude >= 4.7 && < 5
- lorentz
- morley
- morley-prelude
- universum
```

After you have saved the changes to `stack.yaml` and `package.yaml`, run 
`stack build` in `lorentz-contracts` to download the dependencies.

One thing to note is that morley depends on a custom prelude (a replacement for 
the prelude in `base`) called 
[morley-prelude](http://hackage.haskell.org/package/morley-prelude)
that is an extension of another custom prelude called 
[universum](http://hackage.haskell.org/package/universum). It depends on 
[base-noprelude](http://hackage.haskell.org/package/base-noprelude) instead of
[base](http://hackage.haskell.org/package/base).

## Hello Tezos

Now let's make a very simple contract. Let's edit the `Lib.hs` file. Delete
all the lines in the file and add the following:

```haskell
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Lib where

import Lorentz
```

If you have you do not have a lot of Haskell experience, these language pragmas
can be overwhelming. For now, do not worry about them, we will discuss them in 
a later lesson. 

Now let's define the parameter and storage types.

```haskell
type Parameter = ()
type Storage = MText
```

Parameter is set to `unit` and storage is `string`. Lorentz uses its own type
called `MText`, don't use `String` or `Text` from Haskell. These piece of code
is equivalent to the following in Michelson:

```michelson
parameter unit;
storage string;
```

Now we let's define the code body of the contract.

```haskell
helloTezosContract :: ContractCode Parameter Storage
helloTezosContract = do
  drop
  push [mt|Hello Tezos!|]
  nil
  pair
```

`drop` gets rid of the `(Parameter,Storage)` pair that contracts start with on
the top of the stack. `push [mt|...|]` pushes a `MText` string to the top of 
the stack. `MText` is a subset of characters that are permitted in a Tezos 
smart contract. `nil` pushes an empty list. Lorentz will be able to infer the type 
if it is given enough context. Finally, `pair` makes a pair of the two items 
on the top of the stack `([],"Hello Tezos!")`. This matches the expected return
type of `Contract Parameter Storage`, `([Operation], Storage)`.

Notice the type signature of this function. `ContractCode` is a type that has two
type parameters, one for the parameter type and one for the storage type.

Now let's print out the contract as a Michelson file. Edit the executable file
`app/Main.hs`.

```haskell
module Main where

import Lorentz (compileLorentzContract, defaultCompilationOptions, Contract(..))
import Michelson.Printer (printTypedContract)
import System.IO (hSetEncoding, utf8)

import Lib (helloTezosContract)

writeFileUtf8 :: Print text => FilePath -> text -> IO ()
writeFileUtf8 name txt =
  withFile name WriteMode $ \h -> hSetEncoding h utf8 >> hPutStr h txt

main :: IO ()
main = do
  writeFileUtf8 
    "helloTezos.tz"
    (printTypedContract False $
     compileLorentzContract (Contract helloTezosContract False defaultCompilationOptions))
```

`writeFileUtf8` is just a helper function to write a file in `utf8` encoding.
We convert the Lorentz contract into Haskell `Text` and put it into a file 
called `helloTezos.tz`. 

Run `stack build` again and `stack exec hello-lorentz-exe`. Now you should have a
file `helloTezos.tz`. Open it up and check it out. You can even type check it 
with the tezos client.


```
$ tezos-client typecheck script helloTezos.tz
```

## Making reusable functions in Lorentz

One of the best features of Lorentz is being able to split the code into 
functions for specific tasks. It makes the code more readable and allows for
reusability. These are features that do not exist in Michelson.

Let's start with a quick example.

```haskell
addOne :: Natural & s :-> Natural & s
addOne = do
  push @Natural 1
  add
```
Look at the type signature `Natural & s :-> Natural & s`. `:->` 
is an operator that takes the initial state of the stack and the return state
of the stack. `&` connects types of the stack and `s` means the rest of the 
stack, which we ignore in the body of the function, but need to return it. 
In this case the initial state is `Natural & s` and the return state is `Natural & s`.

The body of the function is simple. `push @Natural 1` means push a natural
of value `1` to the top of the stack. Then `add` is called. This function 
requires that a `Natural` is at the top of the stack so given the `Natural` at
the top and `Natural` we pushed, `add` has the two values at the top of the
stack that it needs.

The `@Natural` is a type notation passed to the `push` function to help the 
compiler. If you did `push @Natural [mt||]` it would break. Likewise if you did 
`push 1` and it is not clear to the compiler that `1` should be a `Natural`, 
then it will also fail to compile.

Make a new contract that uses the `addOne` function, print it out, and type 
check it with the tezos client.

```haskell
plusOneContract :: ContractCode Natural Natural
plusOneContract = do
  car
  addOne
  nil
  pair
```

### Exploring Lorentz type signatures

Let's look at some different type signatures. This example requires a stack
that has two values at the top and returns a stack with one at the top.

```haskell
add' :: Natural & Natural & s :-> Natural & s
add' = add
```

This example has a stack that requires no values at the top and returns the 
stack with a new value at the top.

```haskell
helloWorld :: s :-> MText & s
helloWorld = push [mt|Hello World|]
```

And a function where we conver the type. Michelson does not have a built in
function to cast from `Mutez` to `Natural`, but we can make our own.

```haskell
-- add this to the import list
import Tezos.Core (unsafeMkMutez)

mutezToNatural :: Mutez & s :-> Natural & s
mutezToNatural = do
  dip (push (unsafeMkMutez 1))
  ediv
  ifNone (push [mt|mutezToNatural: unexpected error|] # failWith) car
```

In Michelson if you `ediv` a `mutez` by a `mutez`, it returns 
`option (pair nat mutez)` where the `nat` is the result of integer division.
By dividing by one, it converts the `mutez` to a `nat` for us. `unsafeMkMutez` 
is a function from `Tezos.Core`. You give it an integer and it 
returns a `Mutez`, but you are responsible for making sure it is a natural number.
We use `unsafeMkMutez` to push 1 under the `Mutez` in our stack, then we `ediv`
and run a `ifNone` to get rid of the `some` part.

You may have also noticed the `#` function. You have three options for writing a
sequence of operations in Lorentz.

1. `do` and `;`
   If you put a single operation on a single line, you do not need `;`, but if 
   you want multiple operations on a single line, you need to add `;`.
   ```haskell
   doSomething = do
     push @Natural 1; push @Natural 2
     add
     drop
   ```

2. `#`
   This works similarly to `;`, but it does not require `do`.
   ```haskell
   push @Natural 1 # push @Natural 2 # add # drop
   ```
   
3. `>>`
   This is a synonym for `#`.
   ```haskell
   push @Natural 1 >> push @Natural 2 >> add >> drop
   ```

## Challenge

Now let's try to do something non-trivial with Lorentz to consolidate everything
we have learned in this tutorial. There was a 
[question](https://tezos.stackexchange.com/questions/1248/how-to-concatenate-a-string-and-tez-in-liquidity/) 
on the Tezos StackExchange on how to concetenate a `mutez` to a `string`. The 
[answer](https://tezos.stackexchange.com/questions/1248/how-to-concatenate-a-string-and-tez-in-liquidity/1254#1254)
was that you have to use a complicated conversion technique to turn `mutez`, 
`int`, or `nat` into a string.

1. Make a map of single digits to string version: 
   `Map [(0,"0"),(1,"1"),...,(9,"9")]`.
2. With the number `n` we want to convert and an empty string `s`, start a loop.
   `ediv` `n` by `10`, save the result of integer division (the `car` of
   the pair) as `n` and lookup the result of modulus division (the `cdr` of the 
   pair) in the map we made in 1.
3. Concat the result of the lookup to the string `s`. Check if the new `n` is 
   greater than zero. If true then repeat the loop with the new `n` and `s` 
   values. Otherwise, end the loop and return the `s` value as the string form
   of `s`.

First we need to make the map. We want a function with the type signature 
`s :-> Map Natural MText & s`. We take a stack and we put the map on top of
this. We do this by making an empty map, push the string value and wrap it in 
some, push the natural value, and then update the map.

```haskell
mkNatToStringMap :: s :-> Map Natural MText & s
mkNatToStringMap = do
  emptyMap @Natural @MText

  push [mt|0|] >> some
  push @Natural 0
  update

  push [mt|1|] >> some
  push @Natural 1
  update

  push [mt|2|] >> some
  push @Natural 2
  update

  push [mt|3|] >> some
  push @Natural 3
  update

  push [mt|4|] >> some
  push @Natural 4
  update

  push [mt|5|] >> some
  push @Natural 5
  update

  push [mt|6|] >> some
  push @Natural 6
  update

  push [mt|7|] >> some
  push @Natural 7
  update

  push [mt|8|] >> some
  push @Natural 8
  update

  push [mt|9|] >> some
  push @Natural 9
  update
```

In the next function we will introduce a type checking function called 
`stackType`. It is helpful for annotation for telling you what you expect the 
stack to look like at any certain point. If the compiler finds that the stack at 
that point does not match your annotation, it will fail to compile.

```haskell
-- we need 'forall s' because we reference it in the body of the function
naturalToMText :: forall s. Natural & s :-> MText & s
naturalToMText = do
  -- setup the parameters
  dip mkNatToStringMap
  push [mt||] -- empty string to which we will add the results of the lookup to
  push True
  stackType @(Bool : MText : Natural : Map Natural MText : s)

  loop $ do
    stackType @(MText : Natural : Map Natural MText : s)
    -- we do not see the MText for the body of the dip
    dip $ do
      -- get the result of integer division and the modulus
      stackType @(Natural : Map Natural MText : s)
      dip $ push @Natural 10
      ediv
      ifNone (push [mt|naturalToMText: unexpected divide by zero|] # failWith) unpair
      stackType @(Natural : Natural : Map Natural MText : s)

      -- lookup thet modulus result in the map
      -- we do not see the integer division result in the body of the dip 
      dip $ do
        -- now the Natural at the top is the modulus of the division operation
        stackType @(Natural : Map Natural MText : s)
        dip dup -- dup the map, we need to keep it
        get
        ifNone (push [mt|naturalToMText: unexpected get fail|] # failWith) nop

      -- the Natural at the top is the result of integer division
      stackType @(Natural : MText : Map Natural MText : s)
      swap
    stackType @(MText : MText : Natural : Map Natural MText : s)

    -- move the result to the top so it is prepended to the string
    swap
    concat
    
    -- put our number back at the top and check if it is greater than zero
    swap
    stackType @(Natural : MText : Map Natural MText : s)
    
    -- if 0 < n, run the loop again
    dup
    dip $ push @Natural 0
    gt
    
    -- move things around so the order of values is correct
    dip swap
    stackType @(Bool : MText : Natural : Map Natural MText : s)
            
  stackType @( MText : Natural : Map Natural MText : s)
  
  -- we only want to return the MText value
  dip $ drop # drop
```

The notes and the stack type checks in the code should be sufficient to follow
this function's logic.

## Exercise

1. Try adding some of the the functions to the contract in a way that makes the
   type checker break. For example, push an `MText` then call `addOne`. This 
   should help you get familiar with some of the compiler errors for when you 
   make more complex contracts.

2. Use a varying combination of `push`, `dip`, and `drop` and try to match to
   match the stack type at multiple points with `stackType`.

3. Write a contract that concates the input and storage and stores the result
   in the storage.
   
4. The Michelson `or` type is represented in Lorentz as `Either` and the `pair`
   type is represent by the Haskell tuple `(,)`. Create a Lorentz contract that 
   outputs a Michelson contact with the parameter `(or natural string)` and
   the storage type `(pair natural string)`. When the parameter is a `natural`
   it should add the value to the natural in `storage` and store the result. When 
   the parameter is `string` it should append it to the `string` value in
   storage and store the result.

# Appendix A: Lorentz types

These are Haskell data types that are used in Lorentz to represent Michelson
data types.

## Core data types

| Haskell      | Michelson     |
| ------------ | ------------- |
| `MText`      | `string`      |
| `Natural`    | `nat`         |
| `Integer`    | `int`         |
| `ByteString` | `bytes`       |
| `Bool`       | `bool`        |
| `()`         | `unit`        |
| `[v]`        | `list v`      |
| `(a,b)`      | `pair a b`    |
| `Maybe a`    | `option a`    |
| `Either a b` | `or a b`      |
| `Set v`      | `set v`       |
| `Map k v`    | `map k v`     |
| `BigMap k v` | `big_map k v` |
| `Lambda a b` | `lambda a b`  |

## Domain specific data types

| Haskell      | Michelson    |
| ------------ | ------------ |
| `Timestamp`  | `timestamp`  |
| `Mutez`      | `mutez`      |
| `Contract p` | `contract p` |
| `Address`    | `address`    |
| `Operation`  | `operation`  |
| `Key`        | `key`        |
| `KeyHash`    | `key_hash`   |
| `Signature`  | `signature`  |

# Appendix B: Lorentz standard functions

You can find all of the Micheslon equivalent Lorentz functions in this [link](https://gitlab.com/morley-framework/morley/blob/a8f32aba67d8012e1e80ee8b04e4066c5b58607a/src/Lorentz/Instr.hs) along with some necessary utility functions.

| Function             | Description                                                                                                                                                                          |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `nop`                | Equivalent to Michelson's `{}` for empty bodies.                                                                                                                                     |
| `drop`               | Remove the top item of the stack.                                                                                                                                                    |
| `dup`                | Duplicate the top item of the stack.                                                                                                                                                 |
| `swap`               | Swap the top item and the 2nd item from the top.                                                                                                                                     |
| `push`               | Add a value to the top of the stack.                                                                                                                                                 |
| `some`               | Wrap the top value in `some`.                                                                                                                                                        |
| `none`               | Push a `none` to the top of the stack.                                                                                                                                               |
| `unit`               | Push a `unit` to the top of the stack.                                                                                                                                               |
| `ifNone`             | If the top item is none the run the first function body else run the second with the some unwrapped.                                                                                 |
| `pair`               | Put the top two items of the stack into a pair.                                                                                                                                      |
| `car`                | Get the first item of a pair on the top of the stack.                                                                                                                                |
| `cdr`                | Get the second item of a pair on the top of the stack.                                                                                                                               |
| `left`               | Wrap the top item in left.                                                                                                                                                           |
| `right`              | Wrap the top item in right.                                                                                                                                                          |
| `ifLeft`             | If the top item is left then run the first function with left unwrapped else run the second with right unwrapped.                                                                    |
| `nil`                | Push an empty list to the top of the stack.                                                                                                                                          |
| `cons`               | Append an item to a list.                                                                                                                                                            |
| `size`               | Get the size of a list.                                                                                                                                                              |
| `emptySet`           | Push an empty set to the top of the stack.                                                                                                                                           |
| `emptyMap`           | Push an empty map to the top of the stack.                                                                                                                                           |
| `map`                | Apply the body expression to each element of a map.                                                                                                                                  |
| `iter`               | Apply the body expression to all the items of a list.                                                                                                                                |
| `mem`                | Check if an item is a key of a map.                                                                                                                                                  |
| `get`                | Get an item by a key.                                                                                                                                                                |
| `update`             | Update or remove a value in a map by a key.                                                                                                                                          |
| `failingWhenPresent` | Checks whether given key present in the storage and fails if it is. This instruction leaves stack intact.                                                                            |
| `updateNew`          | Like `update`, but throw an error on attempt to overwrite existing entry.                                                                                                            |
| `if_`                | If true then run the first function body else run the second function body.                                                                                                          |
| `ifCons`             |                                                                                                                                                                                      |
| `loop`               | Loop until the top value is `False`.                                                                                                                                                 |
| `loopLeft`           | `loop` with an accumulator.                                                                                                                                                          |
| `lambda`             | Push a function with a given parameter and return type to the top of the stack.                                                                                                      |
| `exec`               | Run a lambda that is at the top of the stack.                                                                                                                                        |
| `dip`                | Run code on the second item of the stack as if it were the first, the top element is protected.                                                                                      |
| `failWith`           | Abort the program                                                                                                                                                                    |
| `cast`               | Ensures the top of the stack has the specified type.                                                                                                                                 |
| `pack`               | Serializes a piece of data to its optimized binary representation.                                                                                                                   |
| `unpack`             | Deserializes a piece of data, if valid.                                                                                                                                              |
| `concat`             | Concat `ByteString` or `MText`.                                                                                                                                                      |
| `concat'`            | Concat the items of a list.                                                                                                                                                          |
| `slice`              | Access `ByteString` or `MText` substring with an offset and a length.                                                                                                                |
| `isNat`              | Check if an `Integer` is a `Natural`.                                                                                                                                                |
| `add`                | Add the following pairs: `(Integer,Integer)`, `(Integer,Natural)`, `(Natural,Integer)`, `(Natural,Natural)`, `(Timestamp,Integer)`, `(Integer,Timestamp)`, `(Mutez,Mutez)`.          |
| `sub`                | Subtract the following pairs: `(Integer,Integer)`, `(Integer,Natural)`, `(Natural,Integer)`, `(Natural,Natural)`, `(Timestamp,Integer)`, `(Timestamp,Timestamp)`, `(Mutez,Mutez)`.   |
| `rsub`               | `sub` but with reversed parameter order. `rsub` with `a & b & s` returns `b - a`.                                                                                                    |
| `mul`                | Multiply the following pairs: `(Integer,Integer)`, `(Integer,Natural)`, `(Natural,Integer)`, `(Natural,Natural)`, `(Mutez,Natural)`, `(Natural,Mutez)`.                              |
| `ediv`               | Perform integer division and modulus on the following pairs: `(Integer,Integer)`, `(Integer,Natural)`, `(Natural,Integer)`, `(Natural,Natural)`, `(Mutez,Natural)`, `(Mutez,Mutez)`. |
| `abs`                | Get the absolute value of an `Integer` as a `Natural`.                                                                                                                               |
| `neg`                | Negate an `Integer` or a `Natural`.                                                                                                                                                  |
| `lsl`                | Logical Shift Left on a `Natural`.                                                                                                                                                   |
| `lsr`                | Logical Shift Right on a `Natural`.                                                                                                                                                  |
| `or`                 | Logical or two `Bool`s.                                                                                                                                                              |
| `and`                | Logical and two `Bool`s.                                                                                                                                                             |
| `xor`                | Logical xor two `Bool`s.                                                                                                                                                             |
| `not`                | Logical not a `Bool`.                                                                                                                                                                |
| `compare`            | Compare two comparable values of the same type.                                                                                                                                      |
| `eq0`                | Check if a value is equal to zero.                                                                                                                                                   |
| `neq0`               | Check if a value is not equal to zero.                                                                                                                                               |
| `lt0`                | Check if a value is less than zero.                                                                                                                                                  |
| `gt0`                | Check if a value is greater than zero.                                                                                                                                               |
| `le0`                | Check if a value is less than or equal to zero.                                                                                                                                      |
| `ge0`                | Check if a value is greater than or equal to zero.                                                                                                                                   |
| `int`                | Convert a `Natural` to an `Integer`.                                                                                                                                                 |
| `self`               | Push the current contract with its type signature.                                                                                                                                   |
| `contract`           | Try to convert an `Address` to a `ContractAddr` of a given type signature.                                                                                                           |
| `transferTokens`     | Transfer tokens to a given contract address. Useful for calling a transaction in another contract.                                                                                   |
| `setDelegate`        | Forge a delegation.                                                                                                                                                                  |
| `createAccount`      | Forge an account (a contract without code).                                                                                                                                          |
| `createContract`     | Forge a new contract.                                                                                                                                                                |
| `implicitAccount`    | Return a default contract with the given public/private key pair.                                                                                                                    |
| `now`                | Push the timestamp of the block whose validation triggered this execution. This value does not change during the transaction.                                                        |
| `amount`             | Get the amount of the current transaction, how much was sent when initiating the transaction.                                                                                        |
| `balance`            | Get the amount of `Mutez` stored in the current contract.                                                                                                                            |
| `checkSignature`     | check that a sequence of bytes has been signed with a given key.                                                                                                                     |
| `sha256`             | Compute a cryptographic hash of the value contents using the Sha256 cryptographic hash function.                                                                                     |
| `sha512`             | Compute a cryptographic hash of the value contents using the Sha512 cryptographic hash function.                                                                                     |
| `blake2B`            | Compute a cryptographic hash of the value contents using the Blake2B cryptographic hash function.                                                                                    |
| `hashKey`            | Compute the b58check of a public key.                                                                                                                                                |
| `stepsToQuota`       | Push the remaining steps before the contract execution must terminate.                                                                                                               |
| `source`             | Push the the contract that paid the fees and storage cost, and whose manager signed the operation that was sent on the blockchain.                                                   |
| `sender`             | Get the contract that pushed the current internal transaction.                                                                                                                       |
| `address`            | Convert a typed `Contract` to an untyped `Address`.                                                                                                                                  |
