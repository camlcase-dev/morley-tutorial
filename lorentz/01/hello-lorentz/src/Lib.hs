{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Lib
  ( helloTezosContract
  , plusOneContract
  , naturalToStringContract
  ) where

import Lorentz

type Parameter = ()

type Storage = MText

helloTezosContract :: ContractCode Parameter Storage
helloTezosContract = do
  drop
  push [mt|Hello Tezos!|]
  nil
  pair

plusOneContract :: ContractCode Natural Natural
plusOneContract = do
  car
  addOne
  nil
  pair

naturalToStringContract :: ContractCode Natural MText
naturalToStringContract = car >> naturalToMText >> nil >> pair

addOne :: Natural & s :-> Natural & s
addOne = do
  push @Natural 1
  add

mkNatToStringMap :: s :-> Map Natural MText & s
mkNatToStringMap = do
  emptyMap @Natural @MText

  push [mt|0|] >> some    
  push @Natural 0
  update

  push [mt|1|] >> some    
  push @Natural 1
  update

  push [mt|2|] >> some    
  push @Natural 2
  update

  push [mt|3|] >> some    
  push @Natural 3
  update

  push [mt|4|] >> some    
  push @Natural 4
  update

  push [mt|5|] >> some    
  push @Natural 5
  update

  push [mt|6|] >> some    
  push @Natural 6
  update

  push [mt|7|] >> some
  push @Natural 7
  update

  push [mt|8|] >> some    
  push @Natural 8
  update

  push [mt|9|] >> some
  push @Natural 9
  update

naturalToMText :: forall s. Natural & s :-> MText & s
naturalToMText = do
  -- setup the parameters
  dip mkNatToStringMap
  push [mt||] -- empty string to which we will add the results of the lookup to
  push True
  stackType @(Bool : MText : Natural : Map Natural MText : s)

  loop $ do
    stackType @(MText : Natural : Map Natural MText : s)
    -- we do not see the MText for the body of the dip
    dip $ do
      -- get the result of integer division and the modulus
      stackType @(Natural : Map Natural MText : s)
      dip $ push @Natural 10
      ediv
      ifNone (push [mt|naturalToMText: unexpected divide by zero|] # failWith) unpair
      stackType @(Natural : Natural : Map Natural MText : s)

      -- lookup thet modulus result in the map
      -- we do not see the integer division result in the body of the dip 
      dip $ do
        -- now the Natural at the top is the modulus of the division operation
        stackType @(Natural : Map Natural MText : s)
        dip dup -- dup the map, we need to keep it
        get
        ifNone (push [mt|naturalToMText: unexpected get fail|] # failWith) nop
      -- the Natural at the top is the result of integer division
      stackType @(Natural : MText : Map Natural MText : s)
      swap
    stackType @(MText : MText : Natural : Map Natural MText : s)
    -- move the result to the top so it is prepended to the string
    swap
    concat
    -- put our number back at the top and check if it is greater than zero
    swap
    stackType @(Natural : MText : Map Natural MText : s)
    -- if 0 < n, run the loop again
    dup
    dip $ push @Natural 0
    gt
    -- move things around so the order of values is correct
    dip swap
    stackType @(Bool : MText : Natural : Map Natural MText : s)
            
  stackType @( MText : Natural : Map Natural MText : s)
  
  -- we only want to return the MText value
  dip $ drop # drop
