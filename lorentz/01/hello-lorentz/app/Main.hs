module Main where

import Lorentz (compileLorentzContract, defaultCompilationOptions, Contract(..))
import Michelson.Printer (printTypedContract)
import System.IO (hSetEncoding, utf8)

import Lib (helloTezosContract, plusOneContract, naturalToStringContract)

writeFileUtf8 :: Print text => FilePath -> text -> IO ()
writeFileUtf8 name txt =
  withFile name WriteMode $ \h -> hSetEncoding h utf8 >> hPutStr h txt

main :: IO ()
main = do
  writeFileUtf8
    "helloTezos.tz"
    (printTypedContract False $
     compileLorentzContract (Contract helloTezosContract False defaultCompilationOptions))

  writeFileUtf8
    "addOne.tz"
    (printTypedContract False $
     compileLorentzContract (Contract plusOneContract False defaultCompilationOptions))

  writeFileUtf8
    "naturalToStringContract.tz"
    (printTypedContract False $
     compileLorentzContract (Contract naturalToStringContract False defaultCompilationOptions))

-- tezos-client typecheck script helloTezos.tz --details
-- tezos-client typecheck script addOne.tz --details
-- tezos-client typecheck script naturalToStringContract.tz --details
