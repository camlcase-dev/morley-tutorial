# Testing Lorentz

This is a short tutorial that goes over how to setup tests for Lorentz smart 
contracts. It assumes you are familiar with the basic principles of unit testing
and property testing in Haskell. If not you should first become familiar with
the `QuickCheck` and `hspec` libraries. The complete example is included at
[./testing-lorentz](./testing-lorentz).

Morley recreates the Michelson smart contract language in Haskell without the 
need to rely on a live Tezos node. This is a huge benefit in terms of iteration
time, but there are a few limitations we will discuss at the end.

Let's start with a quick example.

```haskell
pushErrorMessage :: s :-> MText : s
pushErrorMessage = push [mt|there was an error|]
```

`pushErrorMessage` is a simple function that pushes a string to the top of the
stack. Ideally in a test we would want to do something like this.

```haskell
pushErrorMessage `shouldBe` "there was an error"
```

However, `pushErrorMessage` does not return a normal Haskell value. It exists in
a special environment for emulating a Michelson smart contract so there is some 
setup required to run the function and convert the result to a normal Haskell value.

Here is what our entire unit test will look like for `pushErrorMessage`.

```haskell
  describe "lorentz" $ do
    it "pushErrorMessage" $ do
      let runPushErrorMessage = do 
            let initStack = RNil
            resStack <- interpretLorentzInstr dummyContractEnv pushErrorMessage initStack
            let Identity res :& _ = resStack
            return res
      runPushErrorMessage `shouldBe` Right [mt|there was an error|]
```

We create a function `runPushErrorMessage`. We setup an initial stack that is empty
`let initStack = RNil`. Then we pass a contract environment, the function we want 
to run and the initial stack to `interpretLorentzInstr`. `dummyContractEnv` is
a product type with some initial values for balance, amount, now, etc. that emulates
what a Michelson smart contract can access during run time. We will explore it later.

Finally we do some pattern matching on the expected output from `resStack`. 
`Identity res :& _` matches the top value from the stack and it is returned
as an either value.

Now let's look at a function that takes some input and returns two values on the top
of the stack.

```haskell
moreAndLess :: Integer : s :-> Integer : Integer : s
moreAndLess = do
  dup; push @Integer 1; add
  dip (do dip (push @Integer 1); sub)
```

`moreAndLess` takes a integer and adds one to it and subtracts one from it. The test code
is very similar to `pushErrorMessage`.

```haskell
    it "moreAndLess" $ do
      let runMoreAndLess input = do 
            let initStack = (Identity input :& RNil)
            resStack <- interpretLorentzInstr dummyContractEnv moreAndLess initStack
            let Identity more :& Identity less :& _ = resStack
            return (more, less)

      runMoreAndLess 1  `shouldBe` Right (2,0)
      runMoreAndLess 5  `shouldBe` Right (6,4)
      runMoreAndLess 10 `shouldBe` Right (11,9)
```

Look at `initStack`. Instead of just `RNil` we pass a input value at the top of
the stack `Identity input :& RNil`. Then on the result of resStack, we match the
top two values of the stack and return them in a tuple. `runMoreAndLess` takes
a single input value. 

Following these examples you should be able to write unit tests for your own
Lorentz code.

Let's look at one last example for doing property tests. It only has one small change.

```haskell
mutezToNatural :: Mutez : s :-> Natural : s
mutezToNatural = do
  dip (push @Mutez (unsafeMkMutez 1)); ediv; ifNone (do push [mt||]; failWith) car
```

`mutezToNatural` converts a `Mutez` to a `Natural`. We want to randomly generate `Mutez`
values and check that this function indeed converts them to a `Natural` value of the same
amount.

```haskell
    let runMutezToNatural mutez = do 
          let initStack = (Identity mutez :& RNil)
          resStack <- interpretLorentzInstr dummyContractEnv mutezToNatural initStack
          let Identity res :& _ = resStack
          return res
    
    property $ \x ->
      case mkMutez x of
        Nothing -> True
        Just m  -> runMutezToNatural m == Right (naturalFromInteger . fromIntegral $ x)
```

The setup is the same as the other two examples. We generate an arbitrary integer value, 
then convert it to a `Mutez` and `Natural` on the Haskell side (we are trusting these
are correct). Then we run the Lorentz function and check if it works.

## Limitations

Keep in mind that you do not have an entire Tezos node running and morley does not have a
way to emulate the existence of other contracts on the blockchain. You cannot perform testing
that transfers (calls entrypoints) to other contracts. The work around is split your 
entrypoint functions into smaller functions and test the ones without operations and leave
anything that produces an operation outside of the unit and property tests.

You will still want to test your contract on the test net before originating it on the 
main net.

## Contract Environment

The `ContractEnv` type emulates some values that are determind by the Tezos node
environment at the time a contract is called. You can find it in the morley source code
in the `Michelson.Interpret` module.

```haskell
data ContractEnv = ContractEnv
  { ceNow :: Timestamp
  , ceMaxSteps :: RemainingSteps
  , ceBalance :: Mutez
  , ceContracts :: TcOriginatedContracts
  , ceSelf :: Address
  , ceSource :: Address
  , ceSender :: Address
  , ceAmount :: Mutez
  , ceChainId :: ChainId
  , ceOperationHash :: Maybe U.OperationHash
  }
```

The `Michelson.Test.Dummy` module defines `dummyContractEnv`. You can start
with this value and update the values you need for a test. For example

```haskell
dummyContractEnv { ceBalance = 2000000, ceAmount = 1000000, ceSender = alice }
```

## Exercises

1. Write a property test for `moreAndLess`. 

2. Write a test for the transfer entrypoint of the smart contract from the 
   third tutorial. Remember to split the function up to avoid having 
   `transferTokens` in your tested functions.

3. Try a consecutive unit test where the resulting storage of once function
   acts as the input storage for a second call to another function (could be
   the same one). This is like calling a contract twice in a row on the 
   blockchain.
