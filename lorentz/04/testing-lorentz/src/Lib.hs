{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE TypeSynonymInstances      #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Lib where

import Lorentz
import Tezos.Core (unsafeMkMutez)

pushErrorMessage :: s :-> MText : s
pushErrorMessage = push [mt|there was an error|]

-- | x + 1 : x - 1 : s
moreAndLess :: Integer : s :-> Integer : Integer : s
moreAndLess = do
  dup; push @Integer 1; add
  dip (do dip (push @Integer 1); sub)
  
mutezToNatural :: Mutez : s :-> Natural : s
mutezToNatural = do
  dip (push @Mutez (unsafeMkMutez 1)); ediv; ifNone (do push [mt||]; failWith) car

