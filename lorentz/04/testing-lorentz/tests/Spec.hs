{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Main where

import Lib
import GHC.Natural (naturalFromInteger)
import Lorentz
import           Michelson.Test.Dummy (dummyContractEnv)
import Prelude hiding (abs, (>>))
import Tezos.Core (mkMutez)
import Test.Hspec (Spec, describe, hspec, it, shouldBe)
import           Test.QuickCheck (property)

main :: IO ()
main = do
  hspec propertySpec
  hspec unitSpec

unitSpec :: Spec
unitSpec = do
  describe "lorentz" $ do
    it "pushErrorMessage" $ do
      let runPushErrorMessage = do 
            let initStack = RNil
            resStack <- interpretLorentzInstr dummyContractEnv pushErrorMessage initStack
            let Identity res :& _ = resStack
            return res
      runPushErrorMessage `shouldBe` Right [mt|there was an error|]

    it "moreAndLess" $ do
      let runMoreAndLess input = do 
            let initStack = (Identity input :& RNil)
            resStack <- interpretLorentzInstr dummyContractEnv moreAndLess initStack
            let Identity more :& Identity less :& _ = resStack
            return (more, less)

      runMoreAndLess 1  `shouldBe` Right (2,0)
      runMoreAndLess 5  `shouldBe` Right (6,4)
      runMoreAndLess 10 `shouldBe` Right (11,9)
      
propertySpec :: Spec
propertySpec = do
  it "mutezToNatural" $ do
    let runMutezToNatural mutez = do 
          let initStack = (Identity mutez :& RNil)
          resStack <- interpretLorentzInstr dummyContractEnv mutezToNatural initStack
          let Identity res :& _ = resStack
          return res
    
    property $ \x ->
      case mkMutez x of
        Nothing -> True
        Just m  -> runMutezToNatural m == Right (naturalFromInteger . fromIntegral $ x)
