{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Lib (tokenContract) where

import Lorentz
import Tezos.Core     (unsafeMkMutez)

-- based on this standard with a minor change, no from address for transfer
-- https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-5/tzip-5.md

-- (address :to, nat :value)      %transfer
-- (address :owner, contract nat) %getBalance
-- (unit, contract nat)           %getTotalSupply

type GetTransferParams =
  ("to" :! Address, "value" :! Natural)

type GetTotalBalanceParams =
  ("owner" :! Address, "to" :! ContractRef Natural)

type GetTotalSupplyParams =
  ((), ContractRef Natural)

-- | The three entrypoints of the token contract
data Parameter
  = Transfer       GetTransferParams
  | GetBalance     GetTotalBalanceParams
  | GetTotalSupply GetTotalSupplyParams
  deriving (Generic, IsoValue)

-- | A typeclass that helps morley to convert Parameter intot the entrypoints
instance ParameterHasEntryPoints Parameter where
  type ParameterEntryPointsDerivation Parameter = EpdPlain

-- | The address of who can withdraw the tokens
type Storage =
  ("balances" :! BigMap Address Natural, "totalSupply" :! Natural)

-- | A simple token contract.
tokenContract :: ContractCode Parameter Storage
tokenContract = do
  unpair
  entryCaseSimple @Parameter
    ( #cTransfer       /-> transfer
    , #cGetBalance     /-> getBalance
    , #cGetTotalSupply /-> getTotalSupply
    )

transfer :: Entrypoint GetTransferParams Storage
transfer = do
  -- get sender's balance
  duupX @2; toField #balances; sender; get
  ifNone (do push [mt|sender has no balance|]; failWith) nop
  stackType @[Natural, GetTransferParams, Storage]
  -- assert that the balance is greater than or equal to the transfered amount
  duupX @2; toField #value; dip dup; assertLe [mt|transfered value is greater than the user's balance|]
  stackType @[Natural, GetTransferParams, Storage] -- balance on top
  -- update storage
  dip (getField #value); sub; intToNatural
  stackType @[Natural, GetTransferParams, Storage]
  some; dip (do swap; getField #balances); sender; update; setField #balances;
  stackType @[Storage, GetTransferParams]

  swap; getField #to; dip (do toField #value; some); dip (dip (getField #balances)); update; setField #balances

  nil; pair

intToNatural :: Integer : s :-> Natural : s
intToNatural = do
  dup; push @Integer 0; le; if_ abs (do push [mt||]; failWith)

getBalance :: Entrypoint GetTotalBalanceParams Storage
getBalance = do
  duupX @2; toField #balances; duupX @2; toField #owner; get
  ifNone (do push [mt|owner has no balance|]; failWith) nop
  stackType @[Natural, GetTotalBalanceParams, Storage]
  dip (do toField #to; push @Mutez (unsafeMkMutez 0)); transferTokens;
  dip nil; cons; pair

getTotalSupply :: Entrypoint GetTotalSupplyParams Storage
getTotalSupply = do
  cdr; push @Mutez (unsafeMkMutez 0); duupX @3; toField #totalSupply; transferTokens;
  dip nil; cons; pair
