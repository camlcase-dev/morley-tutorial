module Main where

import Lib (tokenContract)

import Lorentz (compileLorentzContract, defaultCompilationOptions, Contract(..))
import Michelson.Printer (printTypedContract)
import System.IO (hSetEncoding, utf8)

writeFileUtf8 :: Print text => FilePath -> text -> IO ()
writeFileUtf8 name txt =
  withFile name WriteMode $ \h -> hSetEncoding h utf8 >> hPutStr h txt

main :: IO ()
main = do
  writeFileUtf8
    "token.tz"
    (printTypedContract False $
     compileLorentzContract (Contract tokenContract False defaultCompilationOptions))

-- tezos-client typecheck script token.tz --details
