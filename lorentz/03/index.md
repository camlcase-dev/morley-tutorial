# Token Contract in Lorentz

For this third tutorial we will implement a simple version of a token contract
based off of this [standard](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-5/tzip-5.md).
Look at the [Lib.hs](./simple-token/src/Lib.hs) implementation reference. We 
will not go over ever command in that file, but the data provided here should
be sufficient to understand it.

It has three entrypoints:

- (address :from, (address :to, nat :value)) %transfer
- (address :owner, contract nat) %getBalance
- (unit, contract nat) %getTotalSupply

For simplicity we alter the first one to remove the `:from` address.

Transfer exchanges ownership of the provided `value` from the sender
to the `to` address if the sender has at lease that amount. Otherwise it 
will fail.

getBalance returns the balance for a particular address to a provided contract
that has an entrypoint with the type signature `nat`. This is useful for a
contract that depends on this token contract.

getTotalSupply is similar to getBalance except it returns the total amount of
tokens that exist in the contract.

For each of these entrypoints, we will define a type synonym.

```haskell
type GetTransferParams =
  ("to" :! Address, "value" :! Natural)
```

The `:!` is an operator that creates a record like type in lorentz. We will be 
able to get and set these values.

The parameter type for getTotalBalance is similar. `ContractRef` is a contract
that has an entrypoint with the parameter type provided. In this case `Natural`.

```haskell
type GetTotalBalanceParams =
  ("owner" :! Address, "to" :! ContractRef Natural)
```

Finally, for getTotalSupply we won't use the record style. We will just create
a tuple type and in the entrypoint we can drop the unit part of it by calling
`cdr` to get the `ContractRef`.

```haskell
type GetTotalSupplyParams =
  ((), ContractRef Natural)
```

The `Storage` type is also a record synonym.

```haskell
type Storage =
  ("balances" :! BigMap Address Natural, "totalSupply" :! Natural)
```

`balances` is a `BigMap` of addresses to the amount of token they own.
`totalSupply` is the sum of all the `Natural` values in `balances`.

For the rest of the tutorial I will just out interesting commands that
are unique to lorentz and not found in Michelson. Most of the Michelson commands
in the [official documentation](https://tezos.gitlab.io/whitedoc/michelson.html)
are available in lorentz and should be clear if you are already familiar with the
Michelson language.

getTotalSupply is the simplest of the three entrytpoints.

```haskell
getTotalSupply :: Entrypoint GetTotalSupplyParams Storage
getTotalSupply = do
  cdr; push @Mutez (unsafeMkMutez 0); duupX @3; toField #totalSupply; transferTokens;
  dip nil; cons; pair
```

`@Mutez` is a type annotation. It tells the compiler that the expected type is `Mutez`, 
but in this case it is not strictly necessary because the compiler can infer that 
the pushed value is a `Mutez` from the return type of `unsafeMkMutez`.

`unsafeMkMutez` trusts that the user will provide a value between `0` and
`9223372036854775807` or the resulting Michelson contract will not work 
properly. `mkMutez` is a safe version but returns a `Maybe Mutez` and is
not directly useable in lorentz.

`duupX @3` means copy the third item in the stack and put it on top of the stack.
In this case the stack is `[Mutez, ContractRef Natural, Storage]` and after
calling it is `[Storage, Mutez, ContractRef Natural, Storage]`.

`toField` converts any record type synonym like `("a" :! MText, "b" :! Natural)`
into the item at the field. `toField #totalSupply` gets `totalSupply` from `Storage`.
Lorentz converts `toField` into a series of `DIG`, `DUP`, `SWAP`, `CAR` and `CDR`
commands.

The only thing new in `getBalance` is a function called `stackType`. This is a very
useful type for building and debugging functions in lorentz. It says that at this particular
point, the stack should match this. This is only used by lorentz and gets erased when
compiled to Michelson so it is zero cost for your originated contract. You can use it
liberally.

`stackType @[Natural, GetTotalBalanceParams, Storage]` means there is a stack of
three values in this order. You can also used type holes `_`, if you do not care what
a particular type is `stackType @[Natural, _, _]`.

Finally in the `transfer` function there are two new functions.

`getField` is similar to `toField`, but it retrieves a field and duplicates the
record. For example, `getField #value` when `GetTransferParams` is on the top
puts `[Natural, GetTransferParams]` on the top.

`setField` updates a field of a record. In `transfer`, we create a new `balances`
value and update the `balances` field in `Storage`. It expects the new value to
be on top of the record value.a

Exercises:

1. Currently users can transfer XTZ to all of the entrypoints, but the XTZ has 
   no way of being removed from the contract. How would you alter the entrypoints
   to reject any transfer that has an amount greater than zero?

2. How would you change the `Storage` type to include a allowance system.
   Hint: each address has a balance and a mapping of addresses and how much
   they are allowed to spend.
   
3. How would `transfer` change if an allowance system was included.

See [FA1.2 - Approvable Ledger](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md)
for a token standard with an allowance system.
