# Part II: Lorentz Entrypoints

## Adding entrypoints to a contract

Using the project from the first tutorial or starting a new one and copying
the `stack.yaml` file and details from `package.yaml`, we will make a simple
contract that has two entrypoints. Entrypoints are to a smart contract, as 
exposed routes are to an API.

The declaration of the top of the file is similar.

```haskell
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Lib where

import Lorentz
```

The declaration of the contract's parameter type is different. We want two
entrypoints so we define a sum type with two contructors.

```haskell
data Parameter
  = Donate ()
  | Withdraw ()
  deriving (Generic, IsoValue)
```

The `donate` and `withdraw` entrypoints will both take a value of unit `()` 
meaning no extra data is passed to the entrypoints.

We also need to declare this type class instance to help Morley convert the Haskell
code into the correct Michelson parameter type. This can be defined this way for
any normal sum type parameter.

```haskell
instance ParameterHasEntryPoints Parameter where
  type ParameterEntryPointsDerivation Parameter = EpdPlain
```

The storage is very simple. We keep track of an address that will have the right
to withdraw the XTZ held by the smart contract.

```haskell
type Storage = Address
```

The definition of the `donate` entrypoint is simple. When XTZ is sent to an
entrypoint, it is automatically included in the smart contracts balance. We
need to do nothing else but return `([Operation], Storage)` like we saw in
the first tutorial.

```haskell
donate :: Entrypoint () Storage
donate = do
  -- accept the sent XTZ, do nothing else
  drop; nil; pair
```

In the `withdraw` entrypoint, we want to check if the `sender` (address calling
the contract entrypoint) is the same as what is in `Storage`.

First we drop the `()` parameter
```Haskell
withdraw :: Entrypoint () Storage
withdraw = do
  drop
```

Then we duplicate the storage (we will need the address later), push the sender
to the top of the stack and check equality.

```Haskell
  dup; sender; eq
```

We define two code paths with `if_`. If the comparison is true, we want to send
the XTZ held by the contract to `sender`. We need to call `transferTokens` which
expects the following stack `p : mutez : contract p`. We get the `sender` address,
try to convert it to paramter type unit `Lorentz.contract @()`, then check if
we were succesful (if not fail) `ifNone (failWith) nop`.

```Haskell
  if_ (do
          -- prepare to transfer tokens back to the sender
          sender; Lorentz.contract @(); ifNone (failWith) nop;
```

Finally, push the total amount of XTZ held by the contract with `balance` and 
the matching parameter type of the contract `()`. `transferTokens` will send
the `XTZ` and return an `Operation`. Finish the entry point the usual way with
`([Operation], Storage)`.

```Haskell
          balance; push (); transferTokens
          -- put the resulting operation in a list
          nil; swap; cons
          pair
      )
```

To handle the case where the sender is not the manager address, push a message 
then `failWith`.

```Haskell
      (do
          push [mt|Only the manager address can withdraw this contracts XTZ.|]
          failWith
      )
```

Finally, there is a generic structure to create `ContractCode` with entrypoints:

```Haskell
donationContract :: ContractCode Parameter Storage
donationContract = do
  unpair
  entryCaseSimple @Parameter
    ( #cDonate   /-> donate
    , #cWithdraw /-> withdraw
    )
```

`cDonate` will be transformed into `%donate` and `cWithdraw` to `%withdraw`. Try
compiling the code, outputting Michelson and originating it on the testnet.

## Exercise

1. Add a third entrypoint that let's the manager account replace itself
   with another account. This will require a new constructor in `Parameter`
   that takes an `Address` as a parameter instead of `()` like the other ones.
   
2. Add a restriction to `%donate` that does not let the manager account 
   donate money. It should perform a check on sender and if the sender is 
   the manager account it fails.
   
3. Change `Storage` to include a timestamp along with the `Address` (hint: use 
   a tuple). Update `withdraw` so it only let's the manager address wihdraw
   after the timestamp. You will need the `now` function.

4. Update `withdraw` to take an `Address`. This is where the tokens will be
   transferred to. It can be the the manager address itself or a third
   address.
