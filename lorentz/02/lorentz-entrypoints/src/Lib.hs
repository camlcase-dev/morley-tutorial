{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Lib
  ( donationContract
  ) where

import Lorentz

-- | The two entrypoints in the donation contract
data Parameter
  = Donate ()
  | Withdraw ()
  deriving (Generic, IsoValue)

-- | A typeclass that helps morley to convert Parameter intot the entrypoints
instance ParameterHasEntryPoints Parameter where
  type ParameterEntryPointsDerivation Parameter = EpdPlain

-- | The address of who can withdraw the donations
type Storage = Address

donationContract :: ContractCode Parameter Storage
donationContract = do
  unpair
  entryCaseSimple @Parameter
    ( #cDonate   /-> donate
    , #cWithdraw /-> withdraw
    )

donate :: Entrypoint () Storage
donate = do
  -- accept the sent XTZ, do nothing else
  drop; nil; pair

withdraw :: Entrypoint () Storage
withdraw = do
  -- drop the unneeded unit parameter
  drop
  -- check that the sender is the priviledged address that can withdraw
  dup; sender; eq
  if_ (do
          -- prepare to transfer tokens back to the sender
          sender; Lorentz.contract @(); ifNone (failWith) nop;
          balance; push (); transferTokens
          -- put the resulting operation in a list
          nil; swap; cons
          pair
      )
      (do
          push [mt|Only the priviledged address can withdraw this contracts XTZ.|]
          failWith
      )
