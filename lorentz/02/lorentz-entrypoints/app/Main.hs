module Main where

import Lorentz (compileLorentzContract, defaultCompilationOptions, Contract(..))
import Michelson.Printer (printTypedContract)
import System.IO (hSetEncoding, utf8)

import Lib (donationContract)

writeFileUtf8 :: Print text => FilePath -> text -> IO ()
writeFileUtf8 name txt =
  withFile name WriteMode $ \h -> hSetEncoding h utf8 >> hPutStr h txt

main :: IO ()
main = do
  writeFileUtf8
    "donation.tz"
    (printTypedContract False $
     compileLorentzContract (Contract donationContract False defaultCompilationOptions))

-- tezos-client typecheck script donation.tz --details
