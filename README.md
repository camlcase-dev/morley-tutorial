# morley-tutorial

A tutorial for writing smart contracts on Tezos with Morley's EDSL called
Lorentz. These tutorials assume you are already familiar with writing smart 
contracts on Tezos with Michelson. It will not teach basic concepts about 
Tezos blockchain, Michelson or Michelson specific data types. If you are not 
familiar with Michelson, read our 
[Michelson Tutorial](https://gitlab.com/camlcase-dev/michelson-tutorial/). 

The [Morley Haskell package](https://gitlab.com/morley-framework/morley) includes:

- A reimplimentation of Michelson in Haskell
- The Morley extension
- A Morley-to-Michelson transpiler
- Lorentz, a Haskell EDSL for Tezos smart contracts
- A testing framework for Lorentz
- Indigo, a high-level language built on Lorentz (still a work in progress)

Morley is an extension of Michelson. It is a superset of Michelson meaning that
all valid Michelson contracts are also valid Morley contracts, but not the other
way around. Morley allows for code reuse and has testing tools that make it 
easier to code than Michelson.

Lorentz is a Haskell EDSL that allows you to code Tezos smart contracts directly
in Haskell and take advantage of Haskell's ecosystem. Then it can print out a 
Michelson contract that can be originated on the Tezos blockchain with the Tezos
client. Lorentz requires some familiarity with the Haskell language.

## Lorentz Tutorials

### Table of Contents

- 01: [Hello Lorentz](lorentz/01)
- 02: [Lorentz Entrypoints](lorentz/02)
- 03: [Token Contract in Lorentz](lorentz/03)
- 04: [Testing Lorentz](lorentz/04)
